export const products = [
  {
    pId: 0,
    name: 'Phone XL',
    price: 799,
    description: 'A large phone with one of the best screens'
  },
  {
    pId: 1,
    name: 'Phone Mini',
    price: 699,
    description: 'A great phone with one of the best cameras'
  },
  {
    pId: 2,
    name: 'Phone Standard',
    price: 299,
    description: ''
  }
]; 